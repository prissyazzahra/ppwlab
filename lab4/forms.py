from django import forms
from .models import Jadwal

class JadwalKegiatan(forms.Form):
	# DAY_CHOICES = (
	# 	('Mon', 'Monday'),
	# 	('Tue','Tuesday'),
	# 	('Wed','Wednesday'),
	# 	('Thu','Thursday'),
	# 	('Fri','Friday'),
	# 	('Sat','Saturday'),
	# 	('Sun','Sunday')
	# 	)

	day = forms.CharField(label="Day", max_length=20)
	date = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}))
	time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))
	name = forms.CharField(label="Activity Name", max_length=30, required=True)
	location = forms.CharField(label="Location", max_length=30)
	category = forms.CharField(label="Category", max_length=30)
