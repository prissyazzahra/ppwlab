from django.conf.urls import url, path
from . import views

urlpatterns = [
	path('', index, name="index")
	path('', form, name="form")
	path('', art, name="art")
	path('', schedule, name="schedule")
	path('', showschedule, name="showschedule")
	path('', delete, name="delete")
]