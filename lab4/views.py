from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import Context, loader
from .forms import JadwalKegiatan
from .models import Jadwal

def index(request):
	return render(request, 'story3.html')
	
def form(request):
	return render(request, 'form.html')
	
def art(request):
	return render(request, 'story3one.html')

def show_schedule(request):
    response = {}
    jadwal = Jadwal.objects.all()
    response = {
        "jadwal" : jadwal
    }
    return render(request, 'showschedule.html', response)


def schedule(request):
	form = JadwalKegiatan(request.POST or None)
	response = {}
	if(request.method == "POST"):
		if (form.is_valid()):
			day = request.POST.get("day")
			date = request.POST.get("date")
			time = request.POST.get("time")
			name = request.POST.get("name")
			location = request.POST.get("location")
			category = request.POST.get("category")
			Jadwal.objects.create(day=day, date=date, time=time, name=name, location=location, category=category)
			return redirect('show_schedule')
		else:
			response['form'] = form
			return render(request, 'schedule.html', response)
	else:
		response['form'] = form
		return render(request, 'schedule.html', response)

def delete(request):
    jadwal = Jadwal.objects.all().delete()
    return redirect('show_schedule')


# Create your views here.
