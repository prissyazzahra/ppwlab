"""story_4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path,re_path
from lab4.views import form as forms
from lab4.views import index as home
from lab4.views import schedule as jadwal
from lab4.views import show_schedule as show
from lab4.views import delete

urlpatterns = [
    path('admin/', admin.site.urls),
	re_path(r'^$', home, name="index"),
	re_path(r'^form/', forms, name="form"),
    re_path(r'^schedule/', jadwal, name="schedule"),
    re_path(r'^showschedule/', show, name="show_schedule"),
    re_path(r'^delete/', delete, name="delete")
]
